/*
BSD License
Copyright (c) 2013, Tom Everett
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of Tom Everett nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

grammar runc;

/*
This grammar was modified from tinyc grammar
https://github.com/antlr/grammars-v4/tree/master/tinyc
For the purposes of MI-RUN.16 course at CTU in Prague
Fall semester 2018/2019, doupajak
*/

/*
    http://www.iro.umontreal.ca/~felipe/IFT2030-Automne2002/Complements/tinyc.c
*/
program
   : statement +
   ;

statement
   : 'if' paren_expr statement
   | 'if' paren_expr statement 'else' statement
   | 'while' paren_expr statement
   | 'print' expr ';'
   | 'scan' id ';'
   | '{' statement* '}' 
   | expr ';'
   | declaration
   | ';'
   ;

paren_expr
   : '(' expr ')'
   ;

expr
   : test
   | id '=' expr
   ;

test
   : sum
   | sum '<' sum
   | sum '<=' sum
   | sum '==' sum
   | sum '!=' sum
   | sum '>' sum
   | sum '<=' sum
   ;

sum
   : product
   | sum '+' product
   | sum '-' product
   ;

product
   : term
   | product '*' term
   | product '/' term
   | product '%' term
   ;

term
   : id
   | integer
   | '-' integer
   | string
   | paren_expr
   ;

declaration
   : 'int' id '=' expr ';'
   | 'int' id ';'
   | 'string' id '=' expr ';'
   | 'string' id ';'
   ;

id
   : IDENTIFIER
   ;

integer
   : INT
   ;

string
   : STRING_LITERAL
   ;


IDENTIFIER
   : [a-z]+
   ;

STRING_LITERAL
   : '"' ~["\\\r\n]* '"'
   ;


INT
   : [0-9] +
   ;

WS
   : [ \r\n\t] -> skip
;