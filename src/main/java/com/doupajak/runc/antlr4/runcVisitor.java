package com.doupajak.runc.antlr4;

// Generated from runc.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link runcParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface runcVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link runcParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(runcParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(runcParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#paren_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParen_expr(runcParser.Paren_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(runcParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#test}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTest(runcParser.TestContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#sum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSum(runcParser.SumContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#product}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProduct(runcParser.ProductContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(runcParser.TermContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(runcParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(runcParser.IdContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#integer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger(runcParser.IntegerContext ctx);
	/**
	 * Visit a parse tree produced by {@link runcParser#string}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(runcParser.StringContext ctx);
}