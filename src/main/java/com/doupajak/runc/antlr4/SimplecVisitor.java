package com.doupajak.runc.antlr4;

import com.doupajak.runc.truffle.*;

import java.util.*;

import org.antlr.v4.runtime.tree.ParseTree;

public class SimplecVisitor extends runcBaseVisitor<runcNode>{

    private enum VariableType {
        INT,
        STRING
    }
    private Map<String, VariableType> declaredVariables = new HashMap<>();

    @Override
    protected runcNode aggregateResult(runcNode aggregate, runcNode nextResult) {
        return nextResult == null ? aggregate : nextResult;
    }

    private void CompilationError(String errorMessage) {
        System.err.println("COMPILATION ERROR: " + errorMessage);
        System.exit(1);
    }

    private runcNode visitBlockOfStatements(List<runcParser.StatementContext> statements) {
        Deque<runcNode> statementNodes = new LinkedList<>();
        for (runcParser.StatementContext statement: statements) {
            runcNode statementNode = visitStatement(statement);
            statementNodes.addLast(statementNode);
        }
        return new runcBlockNode(statementNodes.toArray(new runcNode[]{}));
    }

    @Override
    public runcNode visitProgram(runcParser.ProgramContext ctx) {
        // program -> statement+
        List<runcParser.StatementContext> statements = ctx.statement();
        return visitBlockOfStatements(statements);
    }

    @Override
    public runcNode visitStatement(runcParser.StatementContext ctx) {
        List<ParseTree> children = ctx.children;
        String firstChildText = children.get(0).getText();

        // statement -> "{" statement* "}"
        if (firstChildText.equals("{")) {
            List<runcParser.StatementContext> statements = ctx.statement();
            return visitBlockOfStatements(statements);
        }

        // statement -> "print" expr ";"
        if (firstChildText.equals("print")) {
            runcNode printArg = visitExpr(ctx.expr());
            return new runcPrintNode(printArg);
        }

        // statement -> "scan" id ";"
        if (firstChildText.equals("scan")) {
            String variableName = ctx.id().IDENTIFIER().getText();
            return new runcScanNode(variableName);
        }

        if (firstChildText.equals("if")) {
            runcNode condition = visitExpr(ctx.paren_expr().expr());
            if (condition instanceof runcStringNode) {
                CompilationError("TYPE MISMATCH: Condition needs to be of int type!");
            }
            runcNode ifStatement = visitStatement(ctx.statement(0));
            // statement -> "if" paren_expr statement
            if (children.size() == 3) {
                return new runcBranchNode(condition, ifStatement);
            }
            // statement -> "if" paren_expr statement "else" statement
            else {
                runcNode elseStatement = visitStatement(ctx.statement(1));
                return new runcBranchNode(condition, ifStatement, elseStatement);
            }
        }

        // statement -> "while" paren_expr statement
        if (firstChildText.equals("while")) {
            runcNode condition = visitExpr(ctx.paren_expr().expr());
            if (condition instanceof runcStringNode) {
                CompilationError("TYPE MISMATCH: Condition needs to be of int type!");
            }
            runcNode body = visitStatement(ctx.statement(0));
            return new runcLoopNode(condition, body);
        }

        runcParser.ExprContext expr = ctx.expr();
        // statement -> expr ";"
        if (children.size() == 2 && expr != null && children.get(1).getText().equals(";")) {
            return visitExpr(expr);
        }

        runcParser.DeclarationContext declaration = ctx.declaration();
        // statement -> declaration
        if (declaration != null) {
            return visitDeclaration(declaration);
        }

        // ERROR
        return null;
    }

    @Override
    public runcNode visitDeclaration(runcParser.DeclarationContext ctx) {
        String variableName = ctx.id().IDENTIFIER().getText();

        // ERROR
        if (declaredVariables.containsKey(variableName)) {
            CompilationError("RE-DECLARATION: Variable " + variableName + " is already declared!");
        }

        List<ParseTree> children = ctx.children;
        if (children.get(0).getText().equals("int")) {
            declaredVariables.put(variableName, VariableType.INT);
            runcParser.ExprContext expr = ctx.expr();
            // declaration -> "int" id "=" expr ";"
            if (expr != null) {
                runcNode assignedValue = visitExpr(expr);
                if (assignedValue instanceof runcStringNode) {
                    CompilationError("TYPE MISMATCH: Cannot declare variable " + variableName + " of type int with a string value!");
                }
                return new runcIntegerDeclarationNode(variableName, assignedValue);
            }
            // declaration -> "int" id ";"
            return new runcIntegerDeclarationNode(variableName, null);
        }
        if (children.get(0).getText().equals("string")) {
            declaredVariables.put(variableName, VariableType.STRING);
            runcParser.ExprContext expr = ctx.expr();
            // declaration -> "string" id "=" expr ";"
            if (expr != null) {
                runcNode assignedValue = visitExpr(expr);
                return new runcStringDeclarationNode(variableName, assignedValue);
            }
            // declaration -> "string" id ";"
            return new runcStringDeclarationNode(variableName, null);
        }

        // ERROR
        return null;
    }

    @Override
    public runcNode visitExpr(runcParser.ExprContext ctx) {
        // expr -> test
        runcParser.TestContext test = ctx.test();
        if (test != null) {
            return visitTest(test);
        }

        runcParser.IdContext variable = ctx.id();
        runcParser.ExprContext assignedValue = ctx.expr();

        // expr -> id "=" expr
        if (variable != null && assignedValue != null) {
            String variableName = variable.IDENTIFIER().getText();
            runcNode assignedValueNode = visitExpr(assignedValue);
            if (! declaredVariables.containsKey(variableName)) {
                CompilationError("ASSIGNMENT: Variable " + variableName + " has not been declared!");
            }
            if (declaredVariables.get(variableName) == VariableType.STRING) {
                return new runcStringAssignmentNode(variableName, assignedValueNode);
            }
            if (declaredVariables.get(variableName) == VariableType.INT && assignedValueNode instanceof runcIntegerNode) {
                return new runcIntegerAssignmentNode(variableName, assignedValueNode);
            }
            if (assignedValueNode instanceof runcStringNode) {
                CompilationError("TYPE MISMATCH: Cannot assign value of type string to variable " + variableName + " of type int!");
            }
        }

        // ERROR
        return null;
    }

    @Override
    public runcNode visitTest(runcParser.TestContext ctx) {
        List<ParseTree> children = ctx.children;

        // test -> sum
        if (children.size() == 1) {
            return visitSum(ctx.sum(0));
        }

        // test -> sum OPERATOR expr
        // OPERATOR: > >= == != < <=
        runcNode left = visitSum(ctx.sum(0));
        runcNode right = visitSum(ctx.sum(1));
        String operatorText = children.get(1).getText();
        if (left instanceof runcStringNode && right instanceof runcStringNode) {
            return new runcStringComparisonNode(operatorText, left, right);
        }
        if (left instanceof runcIntegerNode && right instanceof runcIntegerNode) {
            return new runcIntegerComparisonNode(operatorText, left, right);
        }
        CompilationError("TYPE MISMATCH: Cannot compare a string and an integer!");
        return null;
    }

    @Override
    public runcNode visitSum(runcParser.SumContext ctx) {
        List<ParseTree> children = ctx.children;

        // sum -> product
        if (children.size() == 1) {
            return visitProduct(ctx.product());
        }

        // sum -> sum OPERATOR product
        // OPERATOR: + -
        runcNode left = visitSum(ctx.sum());
        runcNode right = visitProduct(ctx.product());
        String operatorText = children.get(1).getText();
        if (left instanceof runcStringNode || right instanceof runcStringNode) {
            // concatenate the two strings
            if (operatorText.equals("+")) {
                return new runcStringConcatNode(left, right);
            }
            else {
                CompilationError("OPERATOR ERROR: Cannot use operator- with a string!");
            }
        }
        return new runcIntegerOperatorNode(operatorText, left, right);
    }

    @Override
    public runcNode visitProduct (runcParser.ProductContext ctx) {
        List<ParseTree> children = ctx.children;

        // product -> term
        if (children.size() == 1) {
            return visitTerm(ctx.term());
        }

        // product -> product OPERATOR term
        // OPERATOR: * / %
        runcNode left = visitProduct(ctx.product());
        runcNode right = visitTerm(ctx.term());
        String operatorText = children.get(1).getText();
        if (left instanceof runcStringNode || right instanceof runcStringNode) {
            CompilationError("OPERATOR ERROR: Cannot use operator* with a string!");
        }
        return new runcIntegerOperatorNode(operatorText, left, right);
    }

    @Override
    public runcNode visitTerm (runcParser.TermContext ctx) {
        runcParser.IntegerContext integer = ctx.integer();
        if (integer != null) {
            // term -> integer
            if (ctx.children.size() == 1) {
                return visitInteger(integer, true);
            }
            // term -> "-" integer
            return visitInteger(integer, false);
        }

        // term -> id
        runcParser.IdContext id = ctx.id();
        if (id != null) {
            return visitId(id);
        }

        // term -> string
        runcParser.StringContext string = ctx.string();
        if (string != null) {
            return visitString(string);
        }

        // term -> paren_expr
        runcParser.Paren_exprContext paren_expr = ctx.paren_expr();
        if (paren_expr != null) {
            return visitExpr(paren_expr.expr());
        }

        // ERROR
        return null;
    }

    public runcNode visitInteger (runcParser.IntegerContext ctx, boolean isPositive) {
        int value = Integer.parseInt(ctx.INT().getSymbol().getText());
        if (!isPositive) {
            value *= -1;
        }
        return new runcIntegerConstantNode(value);
    }

    @Override
    public runcNode visitInteger (runcParser.IntegerContext ctx) {
        return visitInteger(ctx, true);
    }

    @Override
    public runcNode visitString(runcParser.StringContext ctx) {
        String value = ctx.STRING_LITERAL().getText();
        // get rid of \" at the beginning and end of strings
        return new runcStringConstantNode(value.substring(1, value.length()-1));
    }

    @Override
    public runcNode visitId (runcParser.IdContext ctx) {
        String variableName = ctx.IDENTIFIER().getText();
        if (! declaredVariables.containsKey(variableName)) {
            CompilationError("ACCESS: Variable " + variableName + " has not been declared!");
        }
        if (declaredVariables.get(variableName) == VariableType.INT) {
            return new runcIntegerVariableNode(variableName);
        }
        if (declaredVariables.get(variableName) == VariableType.STRING) {
            return new runcStringVariableNode(variableName);
        }
        return null;
    }
}
