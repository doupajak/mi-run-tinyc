package com.doupajak.runc.antlr4;

// Generated from runc.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link runcParser}.
 */
public interface runcListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link runcParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(runcParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(runcParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(runcParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(runcParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#paren_expr}.
	 * @param ctx the parse tree
	 */
	void enterParen_expr(runcParser.Paren_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#paren_expr}.
	 * @param ctx the parse tree
	 */
	void exitParen_expr(runcParser.Paren_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(runcParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(runcParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#test}.
	 * @param ctx the parse tree
	 */
	void enterTest(runcParser.TestContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#test}.
	 * @param ctx the parse tree
	 */
	void exitTest(runcParser.TestContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#sum}.
	 * @param ctx the parse tree
	 */
	void enterSum(runcParser.SumContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#sum}.
	 * @param ctx the parse tree
	 */
	void exitSum(runcParser.SumContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#product}.
	 * @param ctx the parse tree
	 */
	void enterProduct(runcParser.ProductContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#product}.
	 * @param ctx the parse tree
	 */
	void exitProduct(runcParser.ProductContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(runcParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(runcParser.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(runcParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(runcParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#id}.
	 * @param ctx the parse tree
	 */
	void enterId(runcParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#id}.
	 * @param ctx the parse tree
	 */
	void exitId(runcParser.IdContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#integer}.
	 * @param ctx the parse tree
	 */
	void enterInteger(runcParser.IntegerContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#integer}.
	 * @param ctx the parse tree
	 */
	void exitInteger(runcParser.IntegerContext ctx);
	/**
	 * Enter a parse tree produced by {@link runcParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(runcParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link runcParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(runcParser.StringContext ctx);
}