package com.doupajak.runc;

import com.doupajak.runc.antlr4.runcLexer;
import com.doupajak.runc.antlr4.runcParser;
import com.doupajak.runc.antlr4.SimplecVisitor;
import com.doupajak.runc.truffle.runcNode;
import com.doupajak.runc.truffle.runcRootNode;
import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.Truffle;
import java.io.FileInputStream;
import java.io.IOException;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class CLI {

    public static void main( String[] args ) throws IOException {
        if (args.length == 0) {
            System.err.printf("filename expected.\n");
            System.exit(1);
        }

        runcNode n = parseProgram(args[0]);
        runcRootNode root = new runcRootNode(n);
        CallTarget target = Truffle.getRuntime().createCallTarget(root);
        target.call();
    }

    private static runcNode parseProgram(String fileName) throws RecognitionException, IOException {
        runcLexer l = new runcLexer(new ANTLRInputStream(new FileInputStream(fileName)));
        runcParser p = new runcParser(new CommonTokenStream(l));
        p.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                throw new IllegalStateException("failed to parse at line " + line + " due to " + msg, e);
            }
        });
        SimplecVisitor sv = new SimplecVisitor();
        runcNode n = sv.visit(p.program());
        return n;
    }
}
