package com.doupajak.runc.truffle;

public abstract class runcIntegerNode extends runcValueNode {
    public int value;

    @Override
    public String string() {
        return String.valueOf(value);
    }
}
