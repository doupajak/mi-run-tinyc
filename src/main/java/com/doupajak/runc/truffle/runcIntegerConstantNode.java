package com.doupajak.runc.truffle;

public class runcIntegerConstantNode extends runcIntegerNode {
    public runcIntegerConstantNode(int value) {
        this.value = value;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        // This is a constant integer, its string never changes after being constructed
    }
}
