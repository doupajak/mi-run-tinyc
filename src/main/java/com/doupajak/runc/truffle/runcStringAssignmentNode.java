package com.doupajak.runc.truffle;

public class runcStringAssignmentNode extends runcStringNode {
    private String variableName;
    private runcValueNode assignedValue;

    public runcStringAssignmentNode(String variableName, runcNode assignedValue) {
        this.variableName = variableName;
        this.assignedValue = (runcValueNode)assignedValue;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        assignedValue.execute(globalSpace);
        globalSpace.update(variableName, assignedValue.string());
    }
}
