package com.doupajak.runc.truffle;

public class runcBranchNode extends runcNode {
    private runcIntegerNode condition;
    private runcNode ifStatement;
    private runcNode elseStatement;

    public runcBranchNode(runcNode condition, runcNode ifStatement, runcNode elseStatement) {
        this.condition = (runcIntegerNode)condition;
        this.ifStatement = ifStatement;
        this.elseStatement = elseStatement;
    }

    public runcBranchNode(runcNode condition, runcNode ifStatement) {
        this(condition, ifStatement, null);
    }

    public void execute(GlobalSpace globalSpace) {
        condition.execute(globalSpace);
        if (condition.value != 0) {
            ifStatement.execute(globalSpace);
        }
        else if (elseStatement != null) {
            elseStatement.execute(globalSpace);
        }
    }
}
