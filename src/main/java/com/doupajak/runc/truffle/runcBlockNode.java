package com.doupajak.runc.truffle;

import com.oracle.truffle.api.nodes.ExplodeLoop;

public class runcBlockNode extends runcNode {
    @Children private runcNode[] children;

    public runcBlockNode(runcNode[] children) {
        this.children = children;
    }

    @Override
    @ExplodeLoop
    public void execute(GlobalSpace globalSpace) {
        // Just a dummy for now
        //System.out.println("It works!");
        for (runcNode child: children) {
            child.execute(globalSpace);
        }
    }
}
