package com.doupajak.runc.truffle;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GlobalSpace {

    private final Map<String, Integer> integerVariables = new HashMap<>();
    private final Set<String> uninitializedIntegerVariables = new HashSet<>();
    private final Map<String, String> stringVariables = new HashMap<>();
    private final Set<String> uninitializedStringVariables = new HashSet<>();
    private final Map<String, Integer> stringCountedReferences = new HashMap<>();

    public void declareInt(String variable) {
        uninitializedIntegerVariables.add(variable);
    }

    public void declareString(String variable) {
        uninitializedStringVariables.add(variable);
    }

    public void update(String variable, int value) {
        if (uninitializedIntegerVariables.contains(variable)) {
            uninitializedIntegerVariables.remove(variable);
        }
        integerVariables.put(variable, value);
    }

    private void deallocate(String value) {
        if (stringCountedReferences.containsKey(value)) {
            int count = stringCountedReferences.get(value);
            if (count > 1) {
                stringCountedReferences.put(value, count);
            } else {
                stringCountedReferences.remove(value);
            }
        }
    }

    private void allocate(String value) {
        if (stringCountedReferences.containsKey(value)) {
            stringCountedReferences.put(value, stringCountedReferences.get(value) + 1);
        }
        else {
            stringCountedReferences.put(value, 1);
        }
    }

    public void update(String variable, String value) {
        if (uninitializedStringVariables.contains(variable)) {
            uninitializedStringVariables.remove(variable);
        }
        deallocate(stringVariables.get(variable));
        allocate(value);
        stringVariables.put(variable, value);
    }

    public int getInt(String variable) {
        if (uninitializedIntegerVariables.contains(variable)) {
            return 0;
        }
        return integerVariables.get(variable);
    }

    public String getString(String variable) {
        if (uninitializedStringVariables.contains(variable)) {
            return "";
        }
        return stringVariables.get(variable);
    }

    public boolean isInt(String variable) {
        return integerVariables.containsKey(variable) || uninitializedIntegerVariables.contains(variable);
    }
}