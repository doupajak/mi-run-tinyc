package com.doupajak.runc.truffle;

public class runcIntegerVariableNode extends runcIntegerNode {
    private String variableName;

    public runcIntegerVariableNode(String variableName) {
        this.variableName = variableName;
    }

    public void execute(GlobalSpace globalSpace) {
        value = globalSpace.getInt(variableName);
    }
}
