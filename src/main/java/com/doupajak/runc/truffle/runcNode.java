package com.doupajak.runc.truffle;

import com.oracle.truffle.api.nodes.Node;

public abstract class runcNode extends Node {

    public abstract void execute(GlobalSpace globalSpace);
}
