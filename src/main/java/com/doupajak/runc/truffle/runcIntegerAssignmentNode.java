package com.doupajak.runc.truffle;

public class runcIntegerAssignmentNode extends runcIntegerNode {
    private String variableName;
    private runcIntegerNode assignedValue;

    public runcIntegerAssignmentNode(String variableName, runcNode assignedValue) {
        this.variableName = variableName;
        this.assignedValue = (runcIntegerNode)assignedValue;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        assignedValue.execute(globalSpace);
        globalSpace.update(variableName, assignedValue.value);
    }
}
