package com.doupajak.runc.truffle;

public class runcStringVariableNode extends runcStringNode {
    private String variableName;

    public runcStringVariableNode(String variableName) {
        this.variableName = variableName;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        value = globalSpace.getString(variableName);
    }
}
