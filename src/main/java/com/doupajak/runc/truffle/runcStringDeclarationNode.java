package com.doupajak.runc.truffle;

public class runcStringDeclarationNode extends runcNode {
    private String variableName;
    private runcValueNode assignedValue;

    public runcStringDeclarationNode(String variableName, runcNode assignedValue) {
        this.variableName = variableName;
        this.assignedValue = (runcValueNode)assignedValue;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        if (assignedValue != null) {
            assignedValue.execute(globalSpace);
            globalSpace.update(variableName, assignedValue.string());
        }
        else {
            globalSpace.declareString(variableName);
        }
    }
}
