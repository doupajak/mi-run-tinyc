package com.doupajak.runc.truffle;

public class runcIntegerDeclarationNode extends runcNode {
    private String variableName;
    private runcIntegerNode assignedValue;

    public runcIntegerDeclarationNode(String variableName, runcNode assignedValue) {
        this.variableName = variableName;
        this.assignedValue = (runcIntegerNode)assignedValue;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        if (assignedValue != null) {
            assignedValue.execute(globalSpace);
            globalSpace.update(variableName, assignedValue.value);
        }
        else {
            globalSpace.declareInt(variableName);
        }
    }
}
