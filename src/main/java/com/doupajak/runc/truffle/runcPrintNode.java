package com.doupajak.runc.truffle;

public class runcPrintNode extends runcNode {
    private runcValueNode child;

    public runcPrintNode(runcNode child) {
        this.child = (runcValueNode)child;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        // update string of child
        child.execute(globalSpace);
        System.out.println(child.string());
    }
}
