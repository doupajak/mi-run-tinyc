package com.doupajak.runc.truffle;

import java.util.Scanner;

public class runcScanNode extends runcNode {
    private String variableName;

    public runcScanNode(String variableName) {
        this.variableName = variableName;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        Scanner scanner = new Scanner(System.in);
        if (globalSpace.isInt(variableName)) {
            int scannedNumber = scanner.nextInt();
            globalSpace.update(variableName, scannedNumber);
        }
        else {
            String scannedString = scanner.nextLine();
            globalSpace.update(variableName, scannedString);
        }
    }
}
