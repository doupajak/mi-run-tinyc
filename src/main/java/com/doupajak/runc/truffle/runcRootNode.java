package com.doupajak.runc.truffle;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.ExplodeLoop;
import com.oracle.truffle.api.nodes.RootNode;

public class runcRootNode extends RootNode {
    @Child runcNode child;

    public runcRootNode(runcNode child) {
        super(null);
        this.child= child;
    }

    @Override
    @ExplodeLoop
    public Object execute(VirtualFrame virtualFrame) {
        GlobalSpace globalSpace = new GlobalSpace();
        child.execute(globalSpace);
        return null;
    }
}
