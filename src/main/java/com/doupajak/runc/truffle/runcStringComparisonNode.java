package com.doupajak.runc.truffle;

public class runcStringComparisonNode extends runcIntegerNode {
    String operator;
    runcStringNode left;
    runcStringNode right;

    public runcStringComparisonNode(String operator, runcNode left, runcNode right) {
        this.operator = operator;
        this.left = (runcStringNode)left;
        this.right = (runcStringNode)right;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        left.execute(globalSpace);
        right.execute(globalSpace);
        int comparisonResult = left.value.compareTo(right.value);
        switch (operator) {
            case "<":
                this.value = comparisonResult < 0 ? 1 : 0; break;
            case "<=":
                this.value = comparisonResult <= 0 ? 1 : 0; break;
            case "==":
                this.value = comparisonResult == 0 ? 1 : 0; break;
            case "!=":
                this.value = comparisonResult != 0 ? 1 : 0; break;
            case ">":
                this.value = comparisonResult > 0 ? 1 : 0; break;
            case ">=":
                this.value = comparisonResult >= 0 ? 1 : 0; break;
            default:
                this.value = 0; break;
        }
    }
}
