package com.doupajak.runc.truffle;

public abstract class runcStringNode extends runcValueNode {
    public String value;

    public String string() {
        return value;
    }
}
