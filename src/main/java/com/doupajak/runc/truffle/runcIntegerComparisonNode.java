package com.doupajak.runc.truffle;

public class runcIntegerComparisonNode extends runcIntegerNode {
    private String operator;
    private runcIntegerNode left;
    private runcIntegerNode right;

    public runcIntegerComparisonNode(String operator, runcNode left, runcNode right) {
        this.operator = operator;
        this.left = (runcIntegerNode)left;
        this.right = (runcIntegerNode)right;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        // update values for left and right
        left.execute(globalSpace);
        right.execute(globalSpace);
        switch (operator) {
            case "<":
                this.value = left.value < right.value ? 1 : 0; break;
            case "<=":
                this.value = left.value <= right.value ? 1 : 0; break;
            case "==":
                this.value = left.value == right.value ? 1 : 0; break;
            case "!=":
                this.value = left.value != right.value ? 1 : 0; break;
            case ">":
                this.value = left.value > right.value ? 1 : 0; break;
            case ">=":
                this.value = left.value >= right.value ? 1 : 0; break;
            default:
                this.value = 0; break;
        }
    }
}
