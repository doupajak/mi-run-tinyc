package com.doupajak.runc.truffle;

public class runcStringConcatNode extends runcStringNode {
    runcValueNode left;
    runcValueNode right;

    public runcStringConcatNode(runcNode left, runcNode right) {
        this.left = (runcValueNode)left;
        this.right = (runcValueNode)right;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        left.execute(globalSpace);
        right.execute(globalSpace);
        this.value = left.string().concat(right.string());
    }
}
