package com.doupajak.runc.truffle;

public class runcLoopNode extends runcNode {
    private runcIntegerNode condition;
    private runcNode body;

    public runcLoopNode (runcNode condition, runcNode body) {
        this.condition = (runcIntegerNode)condition;
        this.body = body;
    }

    public void execute(GlobalSpace globalSpace) {
        condition.execute(globalSpace);
        while (condition.value != 0) {
            body.execute(globalSpace);
            condition.execute(globalSpace);
        }
    }
}
