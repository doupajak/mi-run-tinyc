package com.doupajak.runc.truffle;

public class runcIntegerOperatorNode extends runcIntegerNode {
    private String operator;
    private runcIntegerNode left;
    private runcIntegerNode right;

    public runcIntegerOperatorNode(String operator, runcNode left, runcNode right) {
        this.operator = operator;
        this.left = (runcIntegerNode)left;
        this.right = (runcIntegerNode)right;
    }

    @Override
    public void execute(GlobalSpace globalSpace) {
        // update values for left and right
        left.execute(globalSpace);
        right.execute(globalSpace);
        switch (operator) {
            case "+":
                this.value = left.value + right.value; break;
            case "-":
                this.value = left.value - right.value; break;
            case "*":
                this.value = left.value * right.value; break;
            case "/":
                this.value = left.value / right.value; break;
            case "%":
                this.value = left.value % right.value; break;
            default:
                this.value = -1; break;
        }
    }
}
