package com.doupajak.runc.truffle;

public class runcStringConstantNode extends runcStringNode {
    public runcStringConstantNode(String value) {
        this.value = value;
    }

    public void execute(GlobalSpace globalSpace) {
        // This is a constant string, its string doesn't change outside of assignment
    }
}
