package com.doupajak.runc;

import com.doupajak.runc.antlr4.runcLexer;
import com.doupajak.runc.antlr4.runcParser;
import com.doupajak.runc.antlr4.SimplecVisitor;
import com.doupajak.runc.truffle.runcNode;
import com.doupajak.runc.truffle.runcRootNode;
import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.Truffle;

import java.io.*;
import java.nio.charset.StandardCharsets;

import junit.framework.TestCase;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.junit.Assert;
import org.junit.Test;


public class SimpleTest extends TestCase {

    @Test
    public void testHelloWorld() throws Exception {
        runcLexer l = new runcLexer(new ANTLRInputStream(getClass().getResourceAsStream("/helloworld.c")));
        runcParser p = new runcParser(new CommonTokenStream(l));
        p.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                throw new IllegalStateException("failed to parse at line " + line + " due to " + msg, e);
            }
        });
        SimplecVisitor sv = new SimplecVisitor();
        runcNode n = sv.visit(p.program());
        runcRootNode cRoot = new runcRootNode(n);
        CallTarget target = Truffle.getRuntime().createCallTarget(cRoot);

        PrintStream originalOut = System.out;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.setOut(new PrintStream(baos));

            target.call();

            Assert.assertTrue(baos.toString().contains("Hello World"));
        } finally {
            System.setOut(originalOut);
        }
   }

    @Test
    public void testFactorial() throws Exception {
        runcLexer l = new runcLexer(new ANTLRInputStream(getClass().getResourceAsStream("/factorial.c")));
        runcParser p = new runcParser(new CommonTokenStream(l));
        p.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                throw new IllegalStateException("failed to parse at line " + line + " due to " + msg, e);
            }
        });
        SimplecVisitor sv = new SimplecVisitor();
        runcNode n = sv.visit(p.program());
        runcRootNode cRoot = new runcRootNode(n);
        CallTarget target = Truffle.getRuntime().createCallTarget(cRoot);

        PrintStream originalOut = System.out;
        InputStream originalIn = System.in;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.setOut(new PrintStream(baos));
            String input = "7";
            System.setIn(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));

            target.call();

            Assert.assertTrue(baos.toString().contains("Faktorial cisla 7 je 5040"));

            input = "10";
            System.setIn(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));

            target.call();

            Assert.assertTrue(baos.toString().contains("Faktorial cisla 10 je 3628800"));
        } finally {
            System.setOut(originalOut);
            System.setIn(originalIn);
        }
    }

    /*
    @Test
    public void testString() throws Exception {
        runcLexer l = new runcLexer(new ANTLRInputStream(getClass().getResourceAsStream("/string.c")));
        runcParser p = new runcParser(new CommonTokenStream(l));
        p.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                throw new IllegalStateException("failed to parse at line " + line + " due to " + msg, e);
            }
        });
        SimplecVisitor sv = new SimplecVisitor();
        runcNode n = sv.visit(p.program());
        runcRootNode cRoot = new runcRootNode(n);
        CallTarget target = Truffle.getRuntime().createCallTarget(cRoot);

        PrintStream originalOut = System.out;
        InputStream originalIn = System.in;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.setOut(new PrintStream(baos));
            String input = "quit\nz\np\n";
            System.setIn(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));

            target.call();

            Assert.assertTrue(baos.toString().contains("Konec."));
        } finally {
            System.setOut(originalOut);
            System.setIn(originalIn);
        }
    }
    */

    /*
    @Test
    public void testGcdLcm() throws Exception {
        runcLexer l = new runcLexer(new ANTLRInputStream(getClass().getResourceAsStream("/gcd_lcm.c")));
        runcParser p = new runcParser(new CommonTokenStream(l));
        p.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                throw new IllegalStateException("failed to parse at line " + line + " due to " + msg, e);
            }
        });
        SimplecVisitor sv = new SimplecVisitor();
        runcNode n = sv.visit(p.program());
        runcRootNode cRoot = new runcRootNode(n);
        CallTarget target = Truffle.getRuntime().createCallTarget(cRoot);

        PrintStream originalOut = System.out;
        InputStream originalIn = System.in;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.setOut(new PrintStream(baos));
            String input = "21 35";
            System.setIn(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));

            target.call();

            Assert.assertTrue(baos.toString().contains("Nejvetsi spolecny delitel je 7"));
            Assert.assertTrue(baos.toString().contains("Nejmensi spolecny nasobek je 105"));
        } finally {
            System.setOut(originalOut);
            System.setIn(originalIn);
        }
    }
    */
}
