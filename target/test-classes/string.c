int continue = 1;
string a;
string b;
string op;
while (continue) {
	print "Zadej operaci (concat, equals, print, quit):";
	scan op;
	print "Zadej prvni string:";
	scan a;
	print "Zadej druhy string:";
	scan b;
	if (op == "concat") {
		a = a + b;
		print "Spojeny retezec: " + a;	
	}
	else {
		if (op == "equals") {
			int cmp = a == b;
			if (cmp == 0)
				print "Retezce se nerovnaji.";
			else
				print "Retezce se rovnaji.";
		}
		else {
			if (op == "print") {
				print "Prvni retezec: " + a;
				print "Druhy retezec: " + b;
			}
			else {
				if (op == "quit") {					
					print "Konec.";
					continue = 0;
				}
				else
					print "Neplatna operace.";
			}
		}
	}
}